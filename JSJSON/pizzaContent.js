let pizzaHtml = '';

let jsonPizza = [
    {
        "name": "Piccante",
        "price": "16$",
        "id": 1,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Spicy Salami",
            " Chilies",
            " Oregano"
        ],
        "imageUrl": "../Bilder/pizza/piccante.jpg"
    },
    {
        "name": "Giardino",
        "price": "14$",
        "id": 2,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Artichokes",
            " Fresh Mushrooms"
        ],
        "imageUrl": "../Bilder/pizza/giardino.jpg"
    },
    {
        "name": "Prosciutto e funghi",
        "price": "15$",
        "id": 3,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Ham",
            " Fresh Mushrooms",
            " Oregano"
        ],
        "imageUrl": "../Bilder/pizza/prosciuttoefunghi.jpg"
    },
    {
        "name": "Quattro formaggi",
        "price": "13$",
        "id": 4,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Parmesan",
            " Gorgonzola"
        ],
        "imageUrl": "../Bilder/pizza/quattroformaggi.jpg"
    },
    {
        "name": "Quattro stagioni",
        "price": "17$",
        "id": 5,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Ham",
            " Artichokes",
            " Fresh Mushrooms"
        ],
        "imageUrl": "../Bilder/pizza/quattrostagioni.jpg"
    },
    {
        "name": "Stromboli",
        "price": "12$",
        "id": 6,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Fresh Chilies",
            " Olives",
            " Oregano"
        ],
        "imageUrl": "../Bilder/pizza/stromboli.jpg"
    },
    {
        "name": "Verde",
        "price": "13$",
        "id": 7,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Broccoli",
            " Spinach",
            " Oregano"
        ],
        "imageUrl": "../Bilder/pizza/verde.jpg"
    },
    {
        "name": "Rustica",
        "price": "15$",
        "id": 8,
        "ingredients": [
            "Tomato",
            " Mozzarella",
            " Ham",
            " Bacon",
            " Onions",
            " Garlic",
            " Oregano"
        ],
        "imageUrl": "../Bilder/pizza/rustica.jpg"
    }
]

jsonPizza.forEach(function(element) {
  
    let name = element.name;
    let price = element.price;
    let id = element.id;
    let ingredients = element.ingredients;
    let imageUrl = element.imageUrl;

    console.log(id);
    
    pizzaHtml += '<div class="menuItem"> <!--Kontainer für die einzelnen Pizzas und deren Infos--><div class="menuImage"> <!--Bild--><img src="'+imageUrl+'" alt="'+name+'"></div><div class="purchaseContainer"> <!--Name, Preis, Einkaufswagen-Icon--><p>'+name+'</p><div class="menuPrice"><p>'+price+'</p><button class="shoppingCartButton" onclick="addToCart()"><img src="../Bilder/shoppingcart.png" alt="Einkaufswagen"></button></div></div><div class="ingredients"> <!--Beschreibung Pizza--><p>'+ingredients+'</p></div></div>';
})

document.getElementById('mainBottom').innerHTML = pizzaHtml;