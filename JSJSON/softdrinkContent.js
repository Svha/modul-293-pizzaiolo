let softDrinkHtml = '';

let jsonSoftDrink = [
    {
        "name": "Coke",
        "price": "2$",
        "id": 1,
        "imageUrl": "../Bilder/soft drink/coke.jpg",
        "volume": "50cl"
    },
    {
        "name": "Fanta",
        "price": "2$",
        "id": 2,
        "imageUrl": "../Bilder/soft drink/fanta.jpg",
        "volume": "50cl"
    },
    {
        "name": "Pepsi",
        "price": "2$",
        "id": 3,
        "imageUrl": "../Bilder/soft drink/pepsi.jpg",
        "volume": "50cl"
    },
    {
        "name": "Red bull",
        "price": "3$",
        "id": 4,
        "imageUrl": "../Bilder/soft drink/redbull.jpg",
        "volume": "50cl"
    }
]

jsonSoftDrink.forEach(function(element) {
  
    let name = element.name;
    let price = element.price;
    let id = element.id;
    let imageUrl = element.imageUrl;
    let volume = element.volume

    console.log(id);
    
    softDrinkHtml += '<div class="menuItem"> <!--Kontainer für die einzelnen Soft Drinks und deren Infos--><div class="menuImage"> <!--Bild--><img src="'+imageUrl+'" alt="'+name+'"></div><div class="nameSaladSoftDrink"> <!--Name--><p>'+name+'</p></div><div class="purchaseContainer"> <!--Auswahl Volumen, Preis, Einkaufswagen-Icon--><select name="petSize"> <!--Auswahl Volumen--><option value="'+volume+'">'+volume+'</option></select><div class="menuPrice"><p>'+price+'</p><button class="shoppingCartButton" onclick="addToCart()"><img src="../Bilder/shoppingcart.png" alt="Einkaufswagen"></button></div></div></div>';
})

document.getElementById('mainBottom').innerHTML = softDrinkHtml;