let counter = localStorage.getItem('cartCounter') || 0; // Den zuletzt gesetzten Counter setzen, wenn dieser leer ist, ist er Null

// Funktion um den Warenkorb Counter zu erhöhen
function addToCart() {
    counter++;
    document.getElementById('counter').textContent = counter; // Den Counter auf der Seite anzeigen
    localStorage.setItem('cartCounter', counter); // Den Counter speichern um beim neu laden wieder zu holen
}

// Speichert den Counter beim Laden der Seite
document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('counter').textContent = counter;
});

// Funktion um einen Alert auszulösen und den Counter auf 0 zu setzen
function counterButton() {
    if (counter != 0){
        alert('Vielen Dank für Ihre Bestellung'); // Alert wenn etwas im Warenkorb ist
        counter = 0;
        document.getElementById('counter').textContent = counter; // Den Counter auf der Seite anzeigen
        localStorage.setItem('cartCounter', counter); // Den Counter speichern um beim neu laden wieder zu holen
    }
    else{
        alert('Es ist noch nichts im Warenkorb') // Alert wenn nichts im Warenkorb ist
    }    
}

// Funktion für den FeedBack Knopf um auf die "Danke" Seite zu gelangen
function goToLink() {
    // Prüfen ob alle nötigen Antworten gegeben sind (die oberen 4 Antworten müssen gegeben sein)
    let pizzaRating = document.querySelector('input[name="pizza"]:checked'); // prüft den ratio-Input
    let priceRating = document.querySelector('input[name="price"]:checked'); // prüft den ratio-Input
    let nameInput = document.querySelector('input[name="name"]'); // liest den Kontent des Namen-Textfeldes aus
    let emailInput = document.querySelector('input[name="address"]'); // liest den Kontent des Emailadress-Textfeldes aus

    if (pizzaRating && priceRating && nameInput.value.trim() !== '' && emailInput.value.trim() !== '') {
        window.location.href = "thankYou.html";
    } else {
        alert("Bitte fülle die oberen 4 Felder aus"); // Alert wenn nich alle nötigen Antworten gegeben sind
    }
}