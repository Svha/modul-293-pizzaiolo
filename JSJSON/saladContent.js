let saladHtml = '';

let jsonSalad = [
    {
        "name": "Green salad with tomatoes",
        "price": "4$",
        "id": 1,
        "ingredients": [
            "Iceberg lettuce",
            " Tomato"
        ],
        "imageUrl": "../Bilder/salad/GreenSaladWithTomato.jpg"
    },
    {
        "name": "Tomato salad with mozzarella",
        "price": "5$",
        "id": 2,
        "ingredients": [
            "Tomato",
            " Mozzarella"
        ],
        "imageUrl": "../Bilder/salad/TomatoSaladWithMozzarella.jpg"
    },
    {
        "name": "Field salad with egg",
        "price": "4$",
        "id": 3,
        "ingredients": [
            "Field salad",
            " Egg"
        ],
        "imageUrl": "../Bilder/salad/FieldSaladWithEgg.jpg"
    },
    {
        "name": "Rocket with parmesan",
        "price": "5$",
        "id": 4,
        "ingredients": [
            "Rocket",
            " Parmesan"
        ],
        "imageUrl": "../Bilder/salad/RocketWithParmesan.jpg"
    }
]

jsonSalad.forEach(function(element) {
  
    let name = element.name;
    let price = element.price;
    let id = element.id;
    let ingredients = element.ingredients;
    let imageUrl = element.imageUrl;

    console.log(id);
    
    saladHtml += '<div class="menuItem"> <!--Kontainer für die einzelnen Salate und deren Infos--><div class="menuImage"> <!--Bild--><img src="'+imageUrl+'" alt="'+name+'"></div><div class="nameSaladSoftDrink"> <!--Name--><p>'+name+'</p></div><div class="ingredients"> <!--Beschreibung Salat--><p>'+ingredients+'</p></div><div class="purchaseContainer"> <!--Auswahl Sauce, Preis, Einkaufswagen-Icon--><select name="dressings"> <!--Auswahl Sauce--><option>Italian dressing</option><option>French dressing</option></select><div class="menuPrice"><p>'+price+'</p><button class="shoppingCartButton" onclick="addToCart()"><img src="../Bilder/shoppingcart.png" alt="Einkaufswagen"></button></div></div></div>'
})

document.getElementById('mainBottom').innerHTML = saladHtml;